import {Component, Input} from 'angular2/core';

@Component({

  selector: 'notification',

  template: `
	<h1 [hidden]="name!=pattern">{{name}} {{notif}}</h1>
`
})

export class Notification {
  @Input() name: string;
  @Input() pattern: string;
  @Input() notif: string;
}
import {Component} from 'angular2/core';
import {Notification} from './notification';

@Component({

  // Declare the tag name in index.html to where the component attaches
  selector: 'hello-world',
  directives: [Notification],
  template: `
	<!-- data-bind to the input element; store value in yourName -->
	<div><notification [name]="yourName" [notif]="yourNotif" [pattern]="yourPattern"></notification>   </div>
    <label>Please, type your name:</label>
	<input type="text" [(ngModel)]="yourName" placeholder="Fill with name">
	<hr>
	<!-- conditionally display yourName -->
	<label>Choose your pattern:</label>
    <input type="text" [(ngModel)]="yourPattern" placeholder="Fill with pattern">
	<hr>
    <label>Choose your notification:</label>
    <input type="text" [(ngModel)]="yourNotif" placeholder="Fill with notification">
  	<hr> 
	<h1 [hidden]="!yourName || yourName=='Mariusz'">Hello {{yourName}}!</h1>
	<h1 [hidden]="yourName!='Mariusz'"><strong>Mariusz</strong> is lecturing in <strong>C025</strong>!</h1>
`

})

export class HelloWorld {
  // Declaring the variable for binding with initial value
  yourName: string = '';
  yourPattern: string = '';
  yourNotif: string = '';
}
